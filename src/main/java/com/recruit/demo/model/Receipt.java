package com.recruit.demo.model;

import lombok.Data;

import java.util.List;

@Data
public class Receipt {

    private List<Product> products;
    private double salesTaxes;
    private double total;

    @Override
    public String toString() {
        return this.products.toString().replace(", ", "").replace("[", "").replace("]", "") + "\nSales Taxes: " + this.salesTaxes + "\nTotal: " + this.total;
    }
}
