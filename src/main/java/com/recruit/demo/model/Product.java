package com.recruit.demo.model;

import lombok.Data;

@Data
public class Product {

    private int quantity;
    private boolean isImported;
    private boolean isTaxFree;
    private String productName;
    private double price;

    public Product(int quantity, boolean isImported, boolean isTaxFree, String productName, int price) {
        this.quantity = quantity;
        this.isImported = isImported;
        this.isTaxFree = isTaxFree;
        this.productName = productName;
        this.price = price;
    }

    public Product() {
    }

    @Override
    public String toString() {
        return this.quantity + (isImported ? (this.productName.contains("imported") ? "" : "imported") : "") + this.productName + this.price + "\n";
    }
}
