package com.recruit.demo.web.rest;

import com.recruit.demo.service.ReceiptService;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/demo")
@CrossOrigin(origins = "http://localhost:4200")
public class ReceiptResource {

    private final ReceiptService receiptService;

    @Autowired
    public ReceiptResource(ReceiptService receiptService) {
        this.receiptService = receiptService;
    }

    @PostMapping("/receipt")
    public String createReceipt(@NonNull @RequestBody String str) {
        String[] arrayOfProductes = str.split("\n");
        return receiptService.createReceipt(arrayOfProductes, arrayOfProductes.length);
    }
}
