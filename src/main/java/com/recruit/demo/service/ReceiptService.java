package com.recruit.demo.service;

import com.recruit.demo.model.Product;
import com.recruit.demo.model.Receipt;
import lombok.NonNull;
import org.springframework.stereotype.Service;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

@Service
public class ReceiptService {

    private static final DecimalFormat df = new DecimalFormat("0.00");

    public static String createReceipt(@NonNull String[] arrayOfProductes, int arrayLenght) {
        Receipt receipt = new Receipt();
        List<Product> products = new ArrayList<>();
        if(arrayOfProductes == null || arrayLenght < 1) return "Not valid values";
        try {
            for (int i = 0; i < arrayLenght; i++) {
                String[] productArray = arrayOfProductes[i].split(" ");
                Product product = new Product();
                if (arrayOfProductes[i].contains("imported")) {
                    product.setImported(true);
                }
                if (arrayOfProductes[i].contains("chocolate") || arrayOfProductes[i].contains("book") || arrayOfProductes[i].contains("pills")) {
                    product.setTaxFree(true);
                }
                int quantity = 0;
                for (int j = 0; j < productArray.length; j++) {
                    quantity = Integer.parseInt(productArray[0]);
                    if(quantity >= 1){
                        product.setQuantity(quantity);
                    } else {
                        return "Not valid values";
                    }
                }
                product.setPrice(Double.parseDouble(productArray[productArray.length - 1]));
                String[] name = (arrayOfProductes[i].split("(\\d)"));
                product.setProductName(name[1].replace(" at", ":"));
                products.add(product);
            }
            receipt.setProducts(products);
            Receipt finalReceipt = calculateProductTaxes(receipt);
            return finalReceipt.toString().replace("\n\n", "\n");
        } catch (NumberFormatException e) {
            return "Not valid values";
        }
    }

    private static Receipt calculateProductTaxes(Receipt receipt) {
        double salesTaxes = 0.0, total = 0.0, nImported = 0.05, nMisc = 0.10, p = 1.0, price;
        Receipt finalReceipt = new Receipt();
        List<Product> products = new ArrayList<>();
        for (int i = 0; i < receipt.getProducts().size(); i++) {
            Product product = receipt.getProducts().get(i);
            price = product.getPrice();
            if (product.isTaxFree() && product.isImported()) {
                product.setPrice(customRound(product.getPrice() * (nImported + p) * product.getQuantity()));
            } else if (product.isTaxFree() && !product.isImported()) {
                product.setPrice(customRound(product.getPrice() * product.getQuantity()));
            } else if (!product.isTaxFree() && product.isImported()) {
                product.setPrice(customRound(product.getPrice() * (nMisc + nImported + p) * product.getQuantity()));
            } else {
                product.setPrice(customRound(product.getPrice() * (nMisc + p) * product.getQuantity()));
            }
            products.add(product);
            total += product.getPrice();
            salesTaxes += product.getPrice() - price;
        }
        finalReceipt.setSalesTaxes(customRound(salesTaxes));
        finalReceipt.setTotal(total);
        finalReceipt.setProducts(products);
        return finalReceipt;
    }

    public static double customRound(double num) {
        df.setRoundingMode(RoundingMode.UP);
        return Double.parseDouble(df.format((num * 20) / 20.0).replace(",", "."));
    }
}
