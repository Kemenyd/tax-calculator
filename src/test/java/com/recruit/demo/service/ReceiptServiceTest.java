package com.recruit.demo.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ReceiptServiceTest {

    @Test
    void create_emptyString_returnWarningTxt() {
        assertEquals("Not valid values",ReceiptService.createReceipt(new String[]{""},0));
    }

    @Test
    void create_notValidString_returnWarningTxt(){
        assertEquals("Not valid values",ReceiptService.createReceipt(new String[]{"1 not valid text"},1));
    }

    @Test
    void create_notValidQuantity_returnWarningTxt(){
        assertEquals("Not valid values", ReceiptService.createReceipt(new String[]{"0 book at 12.10"}, 1));
    }

    @Test
    void create_correct(){
        assertEquals("1 book: 12.49\nSales Taxes: 0.0\nTotal: 12.49",ReceiptService.createReceipt(new String[]{"1 book at 12.49"},1));
    }
}
